import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree

balance_data = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/balance-scale/balance-scale.data',
                           sep= ',', header= None)

print("Dataset Lenght:: ", len(balance_data))
print("Dataset Shape:: ", balance_data.shape)

print "Dataset:: "
balance_data.head()

## DATA SLICING

X = balance_data.values[:, 1:5]
Y = balance_data.values[:,0]
X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size = 0.3, random_state = 100)

## TRAINING

# with Gini Index
dt_gini = DecisionTreeClassifier(criterion = "gini", random_state = 100,
                               max_depth=3, min_samples_leaf=5)
dt_gini.fit(X_train, y_train)

# with Information Gain
dt_entropy = DecisionTreeClassifier(criterion = "entropy", random_state = 100,
 max_depth=3, min_samples_leaf=5)
dt_entropy.fit(X_train, y_train)


## PREDICT
clf_gini.predict([[4, 4, 3, 3]])

y_pred = dt_gini.predict(X_test)
y_pred


y_pred_en = dt_entropy.predict(X_test)
y_pred_en

